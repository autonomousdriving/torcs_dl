import caffe
import csv
import numpy as np
from PIL import Image
import sys

blob = caffe.proto.caffe_pb2.BlobProto()
data = open("./driving_mean_1F.binaryproto" , 'rb' ).read()
blob.ParseFromString(data)
data = np.array(blob.data)
arr = np.array( caffe.io.blobproto_to_array(blob) )
arr = arr[0]
# np.save("./mean_image.npy", arr[0]) # shape is (210, 280, 3)

np.savetxt("./mean_image_R.txt", arr[0])
np.savetxt("./mean_image_G.txt", arr[1])
np.savetxt("./mean_image_B.txt", arr[2])