package dp;
conforms to de.monticore.lang.monticar.generator.roscpp.RosToEmamTagSchema;

tags Mastercomponent {
    tag mastercomponent.groundTruthAffordance with RosConnection = {topic=(/affordance,std_msgs/Float32MultiArray)};
    tag mastercomponent.speedIn with RosConnection = {topic=(/speed,std_msgs/Float32)};
    tag mastercomponent.commandsOut with RosConnection = {topic=(/commands,std_msgs/Float32MultiArray)};    
}
