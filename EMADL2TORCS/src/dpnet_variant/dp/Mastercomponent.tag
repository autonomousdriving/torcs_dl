package dp;
conforms to de.monticore.lang.monticar.generator.roscpp.RosToEmamTagSchema;

tags Mastercomponent {
    tag mastercomponent.imageIn with RosConnection = {topic=(/camera,std_msgs/UInt8MultiArray)};
    tag mastercomponent.speedIn with RosConnection = {topic=(/speed,std_msgs/Float32)};
    tag mastercomponent.commandsOut with RosConnection = {topic=(/commands,std_msgs/Float32MultiArray)};
    tag mastercomponent.predictedAffordanceOut with RosConnection = {topic=(/predictions,std_msgs/Float32MultiArray)};
}
