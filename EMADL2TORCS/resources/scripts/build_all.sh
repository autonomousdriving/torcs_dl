#!/bin/bash

echo "Building Mastercomponent"
cd Mastercomponent
mkdir -p build
cd build
cmake ..
make -j
cd ../..

echo "Building TORCSComponent"
cd TORCSComponent
./build.sh

echo
echo "Well done!"
echo "Run ./run_all.sh to run all."
