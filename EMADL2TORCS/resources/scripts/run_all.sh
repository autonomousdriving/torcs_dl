#!/bin/bash

xterm -T ROSCORE -e roscore &

echo "Waiting for ROSCORE to start..."
sleep 2

xterm -T TORCSComponent -e TORCSComponent/src/TORCSComponent/build/TORCSComponent &

xterm -T Mastercomponent -e Mastercomponent/build/dp_mastercomponent/coordinator/Coordinator_dp_mastercomponent &
