#include "torcsclient.h"

#include <iostream>
#include <chrono>
#include <thread>
#include <cstring>
#include <sys/shm.h>
#include <assert.h>
#include <ros/console.h>
#include <cv.hpp>
#include <highgui.h>

using namespace torcs;

////////////////////// set up opencv
IplImage* semanticRGB=nullptr;
IplImage* error_bar=nullptr;
IplImage* legend=nullptr;
IplImage* background=nullptr;
CvFont font;
char vi_buf[4];
////////////////////// END set up opencv

TorcsClient::TorcsClient()
{
    gotCommand = false;
    std::cout << "TORCS Client!" << std::endl;

    void *shm = NULL;
    int shmid;

    shmid = shmget((key_t)4567, sizeof(struct shared_use_st), 0666);
    if(shmid < 0)
    {
        //std::cerr << "Failed to get shared memory!" << std::endl;
        ROS_ERROR("Failed to get shared memory!");
    return;
    }

    shm = shmat(shmid, 0, 0);
    if(shm == (void*)-1)
    {
        ROS_ERROR("Failed to shmat()!");
        //std::cerr << "Failed to shmat()!" << std::endl;
        return;
    }

    initVisualization();
    
    connected = true;

    std::cout << "Started shared memory at " << std::hex << shm << std::endl;

    shared = (struct shared_use_st*)shm;
    shared->written = 0; // Equals 1 when screenshot data is written and can bea read
    shared->control = 0; // Seems to be not used by TORCS - was only needed by training framework of the original paper
    shared->pause = 1;   // Set to one to pause execution - was used in original paper
    shared->fast = 0.0;

    shared->dist_L = 0.0;
    shared->dist_R = 0.0;

    shared->toMarking_L = 0.0;
    shared->toMarking_M = 0.0;
    shared->toMarking_R = 0.0;

    shared->dist_LL = 0.0;
    shared->dist_MM = 0.0;
    shared->dist_RR = 0.0;

    shared->toMarking_LL = 0.0;
    shared->toMarking_ML = 0.0;
    shared->toMarking_MR = 0.0;
    shared->toMarking_RR = 0.0;

    shared->toMiddle = 0.0;
    shared->angle = 0.0;
    shared->speed = 0.0;

    shared->steerCmd = 0.0;
    shared->accelCmd = 0.0;
    shared->brakeCmd = 0.0;
}

TorcsClient::~TorcsClient()
{
    cvDestroyWindow("Semantic Visualization");
    cvDestroyWindow("Error Bar");
    cvReleaseImage( &error_bar );
    cvReleaseImage( &semanticRGB );
    cvReleaseImage( &background );
    cvReleaseImage( &legend );
}

void TorcsClient::initVisualization()
{
    semanticRGB=cvCreateImage(cvSize(SEMANTIC_WIDTH,SEMANTIC_HEIGHT),IPL_DEPTH_8U,3);
    error_bar=cvCreateImage(cvSize(640,180),IPL_DEPTH_8U,3);
    legend=cvLoadImage("TORCSComponent/assets/Legend6.png");
    background=cvLoadImage("TORCSComponent/assets/semantic_background_3lane.png");

    cvNamedWindow("Semantic Visualization",1);
    cvNamedWindow("Error Bar",1);
    cvInitFont(&font, CV_FONT_HERSHEY_COMPLEX, 1, 1, 1, 2, 8);
}

void TorcsClient::visualizePredictions(const torcs::Affordance &affordance)
{
    float true_angle = float(shared->angle);
    float true_dist_L = float(shared->dist_L);
    float true_dist_R = float(shared->dist_R);
    float true_toMarking_L = float(shared->toMarking_L);
    float true_toMarking_M = float(shared->toMarking_M);
    float true_toMarking_R = float(shared->toMarking_R);
    float true_dist_LL = float(shared->dist_LL);
    float true_dist_MM = float(shared->dist_MM);
    float true_dist_RR = float(shared->dist_RR);
    float true_toMarking_LL = float(shared->toMarking_LL);
    float true_toMarking_ML = float(shared->toMarking_ML);
    float true_toMarking_MR = float(shared->toMarking_MR);
    float true_toMarking_RR = float(shared->toMarking_RR);

    visualize(affordance.getAngle(), true_angle,
              affordance.getToMarkingML(), true_toMarking_ML,
              affordance.getToMarkingMR(), true_toMarking_MR,
              affordance.getToMarkingM(), true_toMarking_M,
              affordance.getToMarkingLL(), true_toMarking_LL,
              affordance.getToMarkingR(), true_toMarking_R,
              affordance.getToMarkingL(), true_toMarking_L,
              affordance.getToMarkingRR(), true_toMarking_RR,
              affordance.getDistLL(), true_dist_LL,
              affordance.getDistRR(), true_dist_RR,
              affordance.getDistMM(), true_dist_MM,
              affordance.getDistL(), true_dist_L,
              affordance.getDistR(), true_dist_R);
}

void TorcsClient::setPublishCallback(std::function<void(std::vector<uint8_t>, Affordance)> callback)
{
    publishCallback = callback;
}

void TorcsClient::sendCommand(const Command& cmd)
{
    gotCommand = true;
    command = cmd;
}

void TorcsClient::setLanesCount(int count)
{
    lanesCount = count;
    switch(lanesCount)
    {
        case 1:
            cvReleaseImage( &background );
            background=cvLoadImage("TORCSComponent/assets/semantic_background_1lane.png");
        case 2:
            cvReleaseImage( &background );
            background=cvLoadImage("TORCSComponent/assets/semantic_background_2lane.png");
        case 3:
            cvReleaseImage( &background );
            background=cvLoadImage("TORCSComponent/assets/semantic_background_3lane.png");
        default:
            ROS_ERROR("Unexpected lanes count! D:");
    }
}

void TorcsClient::iterate()
{
    assert(connected);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    bool firstRun = true;
    bool firstShot = true;
    bool readyForNewShot = true;
    while (true) {
        if (firstRun) {
            firstRun = false;
            shared->written = 0;
            shared->pause = 1;
            shared->control = 1;
        }
        if (shared->written == 1) { // the new image data is ready to be read
            if (!readyForNewShot)
            {
                shared->written = 0;
            }
            else
            {
                readyForNewShot = false;
                std::vector<uint8_t> screenshot(torcs::IMAGE_SIZE_BYTES);
                memcpy(screenshot.data(), shared->data, screenshot.size());
                shared->written = 0;
                Affordance affordance;
                affordance.populate(*shared); // ground truth affordance
                publishCallback(screenshot, affordance);
                firstShot = false;
            }
        }
        if (gotCommand)
        {
            gotCommand = false;
            readyForNewShot = true;
            //shared->pause = 0;
            //shared->control = 1;
            shared->accelCmd = command.accelCmd;
            shared->steerCmd = command.steerCmd;
            shared->brakeCmd = command.brakeCmd;
        }
    }
#pragma clang diagnostic pop
}

void TorcsClient::visualize(float angle, float true_angle, float toMarking_ML, float true_toMarking_ML,
        float toMarking_MR, float true_toMarking_MR, float toMarking_M, float true_toMarking_M,
        float toMarking_LL, float true_toMarking_LL, float toMarking_R, float true_toMarking_R,
        float toMarking_L, float true_toMarking_L, float toMarking_RR, float true_toMarking_RR,
        float dist_LL, float true_dist_LL, float dist_RR, float true_dist_RR, float dist_MM, float true_dist_MM,
        float dist_L, float true_dist_L, float dist_R, float true_dist_R)
{
    ////////////////////// visualization parameters
    int marking_head=1;
    int marking_st;
    int marking_end;
    int pace;
    int car_pos;
    int goto_lane=0;
    int true_goto_lane=0;

    float p1_x,p1_y,p2_x,p2_y,p3_x,p3_y,p4_x,p4_y;
    CvPoint* pt = new CvPoint[4];
    int visualize_angle=1;

    float err_angle;
    float err_toMarking_ML;
    float err_toMarking_MR;
    float err_toMarking_M;

    int manual=0;
    int counter=0;
    ////////////////////// END visualization parameters

#if DEBUG_LOG
    std::cout << "angle: " << angle << " should be " << true_angle << std::endl; 
    std::cout << "toMarking_ML: " << toMarking_ML << " should be " << true_toMarking_ML << std::endl; 
    std::cout << "toMarking_MR: " << toMarking_MR << " should be " << true_toMarking_MR << std::endl; 
    std::cout << "toMarking_M: " << toMarking_M << " should be " << true_toMarking_M << std::endl; 
    std::cout << "toMarking_LL: " << toMarking_LL << " should be " << true_toMarking_LL << std::endl; 
    std::cout << "toMarking_R: " << toMarking_R << " should be " << true_toMarking_R << std::endl; 
    std::cout << "toMarking_L: " << toMarking_L << " should be " << true_toMarking_L << std::endl; 
    std::cout << "toMarking_RR: " << toMarking_RR << " should be " << true_toMarking_RR << std::endl; 
    std::cout << "dist_LL: " << dist_LL << " should be " << true_dist_LL << std::endl; 
    std::cout << "dist_RR: " << dist_RR << " should be " << true_dist_RR << std::endl; 
    std::cout << "dist_MM: " << dist_MM << " should be " << true_dist_MM << std::endl; 
    std::cout << "dist_L: " << dist_L << " should be " << true_dist_L << std::endl; 
    std::cout << "dist_R: " << dist_R << " should be " << true_dist_R << std::endl; 
#endif

    if (lanesCount == 1)
    {
        //////////////////////////////////////////////// show legend and error bar
        err_angle=(angle-true_angle)*343.8; // full scale +-12 degree
        if (err_angle>72) err_angle=72;
        if (err_angle<-72) err_angle=-72;

        if (true_toMarking_ML>-5 && -toMarking_ML+toMarking_MR<5.5) {
            err_toMarking_ML=(toMarking_ML-true_toMarking_ML)*72; // full scale +-1 meter
            if (err_toMarking_ML>72) err_toMarking_ML=72;
            if (err_toMarking_ML<-72) err_toMarking_ML=-72;
            err_toMarking_MR=(toMarking_MR-true_toMarking_MR)*72; // full scale +-1 meter
            if (err_toMarking_MR>72) err_toMarking_MR=72;
            if (err_toMarking_MR<-72) err_toMarking_MR=-72;
        } else {
            err_toMarking_ML=0;
            err_toMarking_MR=0;
        }

        if (true_toMarking_M<3 && toMarking_M<2) {
            err_toMarking_M=(toMarking_M-true_toMarking_M)*72; // full scale +-1 meter
            if (err_toMarking_M>72) err_toMarking_M=72;
            if (err_toMarking_M<-72) err_toMarking_M=-72;
        } else {
            err_toMarking_M=0;
        }

        int bar_st=26;
        cvCopy(legend,error_bar);
        cvRectangle(error_bar,cvPoint(319,bar_st+0-10),cvPoint(319+err_angle,bar_st+0+10),cvScalar(127,127,127),-1);
        cvRectangle(error_bar,cvPoint(319,bar_st+42-10),cvPoint(319+err_toMarking_ML,bar_st+42+10),cvScalar(190,146,122),-1);
        cvRectangle(error_bar,cvPoint(319,bar_st+84-10),cvPoint(319+err_toMarking_MR,bar_st+84+10),cvScalar(0,121,0),-1);
        cvRectangle(error_bar,cvPoint(319,bar_st+126-10),cvPoint(319+err_toMarking_M,bar_st+126+10),cvScalar(128,0,0),-1);

        cvLine(error_bar,cvPoint(319,bar_st-15),cvPoint(319,bar_st+144),cvScalar(0,0,0),1);

        cvShowImage("Error Bar",error_bar);
        //////////////////////////////////////////////// END show legend and error bar

        ///////////////////////////// semantic visualization
        cvCopy(background,semanticRGB);

        sprintf(vi_buf,"%d",int(shared->speed*3.6));
        cvPutText(semanticRGB,vi_buf,cvPoint(245,85),&font,cvScalar(255,255,255));

        //////////////// visualize true_angle
        if (visualize_angle==1) {
            true_angle=-true_angle;
            p1_x=-14*cos(true_angle)+28*sin(true_angle);
            p1_y=14*sin(true_angle)+28*cos(true_angle);
            p2_x=14*cos(true_angle)+28*sin(true_angle);
            p2_y=-14*sin(true_angle)+28*cos(true_angle);
            p3_x=14*cos(true_angle)-28*sin(true_angle);
            p3_y=-14*sin(true_angle)-28*cos(true_angle);
            p4_x=-14*cos(true_angle)-28*sin(true_angle);
            p4_y=14*sin(true_angle)-28*cos(true_angle);
        }
        //////////////// END visualize true_angle

        /////////////////// draw groundtruth data
        if (true_toMarking_ML>-5) {     // in the lane

            if (true_toMarking_M<2 && true_toMarking_R>6.5)
                car_pos=int((150-(true_toMarking_ML+true_toMarking_MR)*6+174-true_toMarking_M*12)/2);
            else if (true_toMarking_M<2 && true_toMarking_L<-6.5)
                car_pos=int((150-(true_toMarking_ML+true_toMarking_MR)*6+126-true_toMarking_M*12)/2);
            else
                car_pos=int(150-(true_toMarking_ML+true_toMarking_MR)*6);

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                cvFillConvexPoly(semanticRGB,pt,4,cvScalar(0,0,255));
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,0,255),-1);

            if (true_dist_MM<60)
                cvRectangle(semanticRGB,cvPoint(150-14,600-true_dist_MM*12-28),cvPoint(150+14,600-true_dist_MM*12+28),cvScalar(0,255,255),-1);
        }

        else if (true_toMarking_M<3) {        // on the marking
            if (true_toMarking_L<-6.5) {   // left
                car_pos=int(126-true_toMarking_M*12);
                if (true_dist_R<60)
                    cvRectangle(semanticRGB,cvPoint(150-14,600-true_dist_R*12-28),cvPoint(150+14,600-true_dist_R*12+28),cvScalar(0,255,255),-1);
            } else if (true_toMarking_R>6.5) {  // right
                car_pos=int(174-true_toMarking_M*12);
                if (true_dist_L<60)
                    cvRectangle(semanticRGB,cvPoint(150-14,600-true_dist_L*12-28),cvPoint(150+14,600-true_dist_L*12+28),cvScalar(0,255,255),-1);
            }

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                cvFillConvexPoly(semanticRGB,pt,4,cvScalar(0,0,255));
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,0,255),-1);
        }
        /////////////////// END draw groundtruth data

        //////////////// visualize angle
        if (visualize_angle==1) {
            angle=-angle;
            p1_x=-14*cos(angle)+28*sin(angle);
            p1_y=14*sin(angle)+28*cos(angle);
            p2_x=14*cos(angle)+28*sin(angle);
            p2_y=-14*sin(angle)+28*cos(angle);
            p3_x=14*cos(angle)-28*sin(angle);
            p3_y=-14*sin(angle)-28*cos(angle);
            p4_x=-14*cos(angle)-28*sin(angle);
            p4_y=14*sin(angle)-28*cos(angle);
        }
        //////////////// END visualize angle

        /////////////////// draw sensing data
        if (-toMarking_ML+toMarking_MR<5.5) {     // in the lane

            if (toMarking_M<2 && toMarking_R>6)
                car_pos=int((150-(toMarking_ML+toMarking_MR)*6+174-toMarking_M*12)/2);
            else if (toMarking_M<2 && toMarking_L<-6)
                car_pos=int((150-(toMarking_ML+toMarking_MR)*6+126-toMarking_M*12)/2);
            else
                car_pos=int(150-(toMarking_ML+toMarking_MR)*6);

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                int npts=4;
                cvPolyLine(semanticRGB,&pt,&npts,1,1,cvScalar(0,255,0),2,CV_AA);
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,255,0),2);

            if (dist_MM<50)
                cvRectangle(semanticRGB,cvPoint(150-14,600-dist_MM*12-28),cvPoint(150+14,600-dist_MM*12+28),cvScalar(237,99,157),2);
        }

        else if (toMarking_M<2.5) {    // on the marking
            if (toMarking_L<-6) {   // left
                car_pos=int(126-toMarking_M*12);
                if (dist_R<50)
                    cvRectangle(semanticRGB,cvPoint(150-14,600-dist_R*12-28),cvPoint(150+14,600-dist_R*12+28),cvScalar(237,99,157),2);
            } else if (toMarking_R>6) {  // right
                car_pos=int(174-toMarking_M*12);
                if (dist_L<50)
                    cvRectangle(semanticRGB,cvPoint(150-14,600-dist_L*12-28),cvPoint(150+14,600-dist_L*12+28),cvScalar(237,99,157),2);
            }

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                int npts=4;
                cvPolyLine(semanticRGB,&pt,&npts,1,1,cvScalar(0,255,0),2,CV_AA);
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,255,0),2);
        }
        /////////////////// END draw sensing data

        if ((shared->control==0) || (manual==1)) {
            for (int h = 0; h < SEMANTIC_HEIGHT; h++) {
                for (int w = 0; w < SEMANTIC_WIDTH; w++) {
                    semanticRGB->imageData[(h*SEMANTIC_WIDTH+w)*3+1]=0;
                    semanticRGB->imageData[(h*SEMANTIC_WIDTH+w)*3+0]=0;
                }
            }
        }

        cvShowImage("Semantic Visualization",semanticRGB);
        ///////////////////////////// END semantic visualization
    }
    else if (lanesCount == 2)
    {
        //////////////////////////////////////////////// show legend and error bar
        err_angle=(angle-true_angle)*343.8; // full scale +-12 degree
        if (err_angle>72) err_angle=72;
        if (err_angle<-72) err_angle=-72;

        if (true_toMarking_ML>-5 && -toMarking_ML+toMarking_MR<5.5) {
            err_toMarking_ML=(toMarking_ML-true_toMarking_ML)*72; // full scale +-1 meter
            if (err_toMarking_ML>72) err_toMarking_ML=72;
            if (err_toMarking_ML<-72) err_toMarking_ML=-72;
            err_toMarking_MR=(toMarking_MR-true_toMarking_MR)*72; // full scale +-1 meter
            if (err_toMarking_MR>72) err_toMarking_MR=72;
            if (err_toMarking_MR<-72) err_toMarking_MR=-72;
        } else {
            err_toMarking_ML=0;
            err_toMarking_MR=0;
        }

        if (true_toMarking_M<3 && toMarking_M<2) {
            err_toMarking_M=(toMarking_M-true_toMarking_M)*72; // full scale +-1 meter
            if (err_toMarking_M>72) err_toMarking_M=72;
            if (err_toMarking_M<-72) err_toMarking_M=-72;
        } else {
            err_toMarking_M=0;
        }

        int bar_st=26;
        cvCopy(legend,error_bar);
        cvRectangle(error_bar,cvPoint(319,bar_st+0-10),cvPoint(319+err_angle,bar_st+0+10),cvScalar(127,127,127),-1);
        cvRectangle(error_bar,cvPoint(319,bar_st+42-10),cvPoint(319+err_toMarking_ML,bar_st+42+10),cvScalar(190,146,122),-1);
        cvRectangle(error_bar,cvPoint(319,bar_st+84-10),cvPoint(319+err_toMarking_MR,bar_st+84+10),cvScalar(0,121,0),-1);
        cvRectangle(error_bar,cvPoint(319,bar_st+126-10),cvPoint(319+err_toMarking_M,bar_st+126+10),cvScalar(128,0,0),-1);

        cvLine(error_bar,cvPoint(319,bar_st-15),cvPoint(319,bar_st+144),cvScalar(0,0,0),1);

        cvShowImage("Error Bar",error_bar);
        //////////////////////////////////////////////// END show legend and error bar

        ///////////////////////////// semantic visualization
        cvCopy(background,semanticRGB);

        pace=int(shared->speed*1.2);
        if (pace>50) pace=50;

        marking_head=marking_head+pace;
        if (marking_head>0) marking_head=marking_head-110;
        else if (marking_head<-110) marking_head=marking_head+110;

        marking_st=marking_head;
        marking_end=marking_head+55;

        while (marking_st<=660) {
            cvLine(semanticRGB,cvPoint(150,marking_st),cvPoint(150,marking_end),cvScalar(255,255,255),2);
            marking_st=marking_st+110;
            marking_end=marking_end+110;
        }

        sprintf(vi_buf,"%d",int(shared->speed*3.6));
        cvPutText(semanticRGB,vi_buf,cvPoint(245,85),&font,cvScalar(255,255,255));

        //////////////// visualize true_angle
        if (visualize_angle==1) {
            true_angle=-true_angle;
            p1_x=-14*cos(true_angle)+28*sin(true_angle);
            p1_y=14*sin(true_angle)+28*cos(true_angle);
            p2_x=14*cos(true_angle)+28*sin(true_angle);
            p2_y=-14*sin(true_angle)+28*cos(true_angle);
            p3_x=14*cos(true_angle)-28*sin(true_angle);
            p3_y=-14*sin(true_angle)-28*cos(true_angle);
            p4_x=-14*cos(true_angle)-28*sin(true_angle);
            p4_y=14*sin(true_angle)-28*cos(true_angle);
        }
        //////////////// END visualize true_angle

        /////////////////// draw groundtruth data
        if (true_toMarking_LL>-9) {     // right lane

            if (true_toMarking_M<2 && true_toMarking_R>6.5)
                car_pos=int((174-(true_toMarking_ML+true_toMarking_MR)*6+198-true_toMarking_M*12)/2);
            else if (true_toMarking_M<2 && true_toMarking_R<6.5)
                car_pos=int((174-(true_toMarking_ML+true_toMarking_MR)*6+150-true_toMarking_M*12)/2);
            else
                car_pos=int(174-(true_toMarking_ML+true_toMarking_MR)*6);

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                cvFillConvexPoly(semanticRGB,pt,4,cvScalar(0,0,255));
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,0,255),-1);

            if (true_dist_LL<60)
                cvRectangle(semanticRGB,cvPoint(126-14,600-true_dist_LL*12-28),cvPoint(126+14,600-true_dist_LL*12+28),cvScalar(0,255,255),-1);
            if (true_dist_MM<60)
                cvRectangle(semanticRGB,cvPoint(174-14,600-true_dist_MM*12-28),cvPoint(174+14,600-true_dist_MM*12+28),cvScalar(0,255,255),-1);
        }

        else if (true_toMarking_RR<9) {   // left lane

            if (true_toMarking_M<2 && true_toMarking_L<-6.5)
                car_pos=int((126-(true_toMarking_ML+true_toMarking_MR)*6+102-true_toMarking_M*12)/2);
            else if (true_toMarking_M<2 && true_toMarking_L>-6.5)
                car_pos=int((126-(true_toMarking_ML+true_toMarking_MR)*6+150-true_toMarking_M*12)/2);
            else
                car_pos=int(126-(true_toMarking_ML+true_toMarking_MR)*6);

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                cvFillConvexPoly(semanticRGB,pt,4,cvScalar(0,0,255));
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,0,255),-1);

            if (true_dist_MM<60)
                cvRectangle(semanticRGB,cvPoint(126-14,600-true_dist_MM*12-28),cvPoint(126+14,600-true_dist_MM*12+28),cvScalar(0,255,255),-1);
            if (true_dist_RR<60)
                cvRectangle(semanticRGB,cvPoint(174-14,600-true_dist_RR*12-28),cvPoint(174+14,600-true_dist_RR*12+28),cvScalar(0,255,255),-1);
        }

        else if (true_toMarking_M<3) {
            if (true_toMarking_L<-6.5) {   // left
                car_pos=int(102-true_toMarking_M*12);
                if (true_dist_R<60)
                    cvRectangle(semanticRGB,cvPoint(126-14,600-true_dist_R*12-28),cvPoint(126+14,600-true_dist_R*12+28),cvScalar(0,255,255),-1);
            } else if (true_toMarking_R>6.5) {  // right
                car_pos=int(198-true_toMarking_M*12);
                if (true_dist_L<60)
                    cvRectangle(semanticRGB,cvPoint(174-14,600-true_dist_L*12-28),cvPoint(174+14,600-true_dist_L*12+28),cvScalar(0,255,255),-1);
            } else {
                car_pos=int(150-true_toMarking_M*12);
                if (true_dist_L<60)
                    cvRectangle(semanticRGB,cvPoint(126-14,600-true_dist_L*12-28),cvPoint(126+14,600-true_dist_L*12+28),cvScalar(0,255,255),-1);
                if (true_dist_R<60)
                    cvRectangle(semanticRGB,cvPoint(174-14,600-true_dist_R*12-28),cvPoint(174+14,600-true_dist_R*12+28),cvScalar(0,255,255),-1);
            }

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                cvFillConvexPoly(semanticRGB,pt,4,cvScalar(0,0,255));
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,0,255),-1);
        }
        /////////////////// END draw groundtruth data

        //////////////// visualize angle
        if (visualize_angle==1) {
            angle=-angle;
            p1_x=-14*cos(angle)+28*sin(angle);
            p1_y=14*sin(angle)+28*cos(angle);
            p2_x=14*cos(angle)+28*sin(angle);
            p2_y=-14*sin(angle)+28*cos(angle);
            p3_x=14*cos(angle)-28*sin(angle);
            p3_y=-14*sin(angle)-28*cos(angle);
            p4_x=-14*cos(angle)-28*sin(angle);
            p4_y=14*sin(angle)-28*cos(angle);
        }
        //////////////// END visualize angle

        /////////////////// draw sensing data
        if (toMarking_LL>-8 && toMarking_RR>8 && -toMarking_ML+toMarking_MR<5.5) {     // right lane

            if (toMarking_M<1.5 && toMarking_R>6)
                car_pos=int((174-(toMarking_ML+toMarking_MR)*6+198-toMarking_M*12)/2);
            else if (toMarking_M<1.5 && toMarking_R<=6)
                car_pos=int((174-(toMarking_ML+toMarking_MR)*6+150-toMarking_M*12)/2);
            else
                car_pos=int(174-(toMarking_ML+toMarking_MR)*6);

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                int npts=4;
                cvPolyLine(semanticRGB,&pt,&npts,1,1,cvScalar(0,255,0),2,CV_AA);
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,255,0),2);

            if (dist_LL<50)
                cvRectangle(semanticRGB,cvPoint(126-14,600-dist_LL*12-28),cvPoint(126+14,600-dist_LL*12+28),cvScalar(237,99,157),2);
            if (dist_MM<50)
                cvRectangle(semanticRGB,cvPoint(174-14,600-dist_MM*12-28),cvPoint(174+14,600-dist_MM*12+28),cvScalar(237,99,157),2);
        }

        else if (toMarking_RR<8 && toMarking_LL<-8 && -toMarking_ML+toMarking_MR<5.5) {   // left lane

            if (toMarking_M<1.5 && toMarking_L<-6)
                car_pos=int((126-(toMarking_ML+toMarking_MR)*6+102-toMarking_M*12)/2);
            else if (toMarking_M<1.5 && toMarking_L>=-6)
                car_pos=int((126-(toMarking_ML+toMarking_MR)*6+150-toMarking_M*12)/2);
            else
                car_pos=int(126-(toMarking_ML+toMarking_MR)*6);

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                int npts=4;
                cvPolyLine(semanticRGB,&pt,&npts,1,1,cvScalar(0,255,0),2,CV_AA);
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,255,0),2);

            if (dist_MM<50)
                cvRectangle(semanticRGB,cvPoint(126-14,600-dist_MM*12-28),cvPoint(126+14,600-dist_MM*12+28),cvScalar(237,99,157),2);
            if (dist_RR<50)
                cvRectangle(semanticRGB,cvPoint(174-14,600-dist_RR*12-28),cvPoint(174+14,600-dist_RR*12+28),cvScalar(237,99,157),2);
        }

        else if (toMarking_M<2.5) {
            if (toMarking_L<-6) {   // left
                car_pos=int(102-toMarking_M*12);
                if (dist_R<50)
                    cvRectangle(semanticRGB,cvPoint(126-14,600-dist_R*12-28),cvPoint(126+14,600-dist_R*12+28),cvScalar(237,99,157),2);
            } else if (toMarking_R>6) {  // right
                car_pos=int(198-toMarking_M*12);
                if (dist_L<50)
                    cvRectangle(semanticRGB,cvPoint(174-14,600-dist_L*12-28),cvPoint(174+14,600-dist_L*12+28),cvScalar(237,99,157),2);
            } else if (toMarking_R<6 && toMarking_L>-6) {
                car_pos=int(150-toMarking_M*12);
                if (dist_L<50)
                    cvRectangle(semanticRGB,cvPoint(126-14,600-dist_L*12-28),cvPoint(126+14,600-dist_L*12+28),cvScalar(237,99,157),2);
                if (dist_R<50)
                    cvRectangle(semanticRGB,cvPoint(174-14,600-dist_R*12-28),cvPoint(174+14,600-dist_R*12+28),cvScalar(237,99,157),2);
            }

            if (visualize_angle==1) {
                pt[0] = cvPoint(p1_x+car_pos,p1_y+600);
                pt[1] = cvPoint(p2_x+car_pos,p2_y+600);
                pt[2] = cvPoint(p3_x+car_pos,p3_y+600);
                pt[3] = cvPoint(p4_x+car_pos,p4_y+600);
                int npts=4;
                cvPolyLine(semanticRGB,&pt,&npts,1,1,cvScalar(0,255,0),2,CV_AA);
            } else
                cvRectangle(semanticRGB,cvPoint(car_pos-14,600-28),cvPoint(car_pos+14,600+28),cvScalar(0,255,0),2);
        }
        /////////////////// END draw sensing data

        if ((shared->control==0) || (manual==1)) {
            for (int h = 0; h < SEMANTIC_HEIGHT; h++) {
                for (int w = 0; w < SEMANTIC_WIDTH; w++) {
                    semanticRGB->imageData[(h*SEMANTIC_WIDTH+w)*3+1]=0;
                    semanticRGB->imageData[(h*SEMANTIC_WIDTH+w)*3+0]=0;
                }
            }
        }

        cvShowImage("Semantic Visualization",semanticRGB);
        ///////////////////////////// END semantic visualization
    }
    else if (lanesCount == 3)
    {
        //////////////////////////////////////////////// show legend and error bar
        err_angle = (angle - true_angle) * 343.8; // full scale +-12 degree
        if (err_angle > 72) err_angle = 72;
        if (err_angle < -72) err_angle = -72;

        if (true_toMarking_ML > -5 && -toMarking_ML + toMarking_MR < 5.5) {
            err_toMarking_ML = (toMarking_ML - true_toMarking_ML) * 72; // full scale +-1 meter
            if (err_toMarking_ML > 72) err_toMarking_ML = 72;
            if (err_toMarking_ML < -72) err_toMarking_ML = -72;
            err_toMarking_MR = (toMarking_MR - true_toMarking_MR) * 72; // full scale +-1 meter
            if (err_toMarking_MR > 72) err_toMarking_MR = 72;
            if (err_toMarking_MR < -72) err_toMarking_MR = -72;
        } else {
            err_toMarking_ML = 0;
            err_toMarking_MR = 0;
        }

        if (true_toMarking_M < 3 && toMarking_M < 2) {
            err_toMarking_M = (toMarking_M - true_toMarking_M) * 72; // full scale +-1 meter
            if (err_toMarking_M > 72) err_toMarking_M = 72;
            if (err_toMarking_M < -72) err_toMarking_M = -72;
        } else {
            err_toMarking_M = 0;
        }

        int bar_st = 26;
        cvCopy(legend, error_bar);
        cvRectangle(error_bar, cvPoint(319, bar_st + 0 - 10), cvPoint(319 + err_angle, bar_st + 0 + 10),
                    cvScalar(127, 127, 127), -1);
        cvRectangle(error_bar, cvPoint(319, bar_st + 42 - 10), cvPoint(319 + err_toMarking_ML, bar_st + 42 + 10),
                    cvScalar(190, 146, 122), -1);
        cvRectangle(error_bar, cvPoint(319, bar_st + 84 - 10), cvPoint(319 + err_toMarking_MR, bar_st + 84 + 10),
                    cvScalar(0, 121, 0), -1);
        cvRectangle(error_bar, cvPoint(319, bar_st + 126 - 10), cvPoint(319 + err_toMarking_M, bar_st + 126 + 10),
                    cvScalar(128, 0, 0), -1);

        cvLine(error_bar, cvPoint(319, bar_st - 15), cvPoint(319, bar_st + 144), cvScalar(0, 0, 0), 1);

        cvShowImage("Error Bar", error_bar);
        //////////////////////////////////////////////// END show legend and error bar

        //////////////////////////////////////////////// semantic visualization
        cvCopy(background, semanticRGB);

        pace = int(shared->speed * 1.2);
        if (pace > 50) pace = 50;

        marking_head = marking_head + pace;
        if (marking_head > 0) marking_head = marking_head - 110;
        else if (marking_head < -110) marking_head = marking_head + 110;

        marking_st = marking_head;
        marking_end = marking_head + 55;

        while (marking_st <= 660) {
            cvLine(semanticRGB, cvPoint(126, marking_st), cvPoint(126, marking_end), cvScalar(255, 255, 255), 2);
            cvLine(semanticRGB, cvPoint(174, marking_st), cvPoint(174, marking_end), cvScalar(255, 255, 255), 2);
            marking_st = marking_st + 110;
            marking_end = marking_end + 110;
        }

        sprintf(vi_buf, "%d", int(shared->speed * 3.6));
        cvPutText(semanticRGB, vi_buf, cvPoint(245, 85), &font, cvScalar(255, 255, 255));

        //////////////// visualize true_angle
        if (visualize_angle == 1) {
            true_angle = -true_angle;
            p1_x = -14 * cos(true_angle) + 28 * sin(true_angle);
            p1_y = 14 * sin(true_angle) + 28 * cos(true_angle);
            p2_x = 14 * cos(true_angle) + 28 * sin(true_angle);
            p2_y = -14 * sin(true_angle) + 28 * cos(true_angle);
            p3_x = 14 * cos(true_angle) - 28 * sin(true_angle);
            p3_y = -14 * sin(true_angle) - 28 * cos(true_angle);
            p4_x = -14 * cos(true_angle) - 28 * sin(true_angle);
            p4_y = 14 * sin(true_angle) - 28 * cos(true_angle);
        }
        //////////////// END visualize true_angle

        /////////////////// draw groundtruth data
        if (true_toMarking_LL > -9 && true_toMarking_RR > 9) {     // right lane

            if (true_toMarking_M < 2 && true_toMarking_R > 6)
                car_pos = int((198 - (true_toMarking_ML + true_toMarking_MR) * 6 + 222 - true_toMarking_M * 12) / 2);
            else if (true_toMarking_M < 2 && true_toMarking_R < 6)
                car_pos = int((198 - (true_toMarking_ML + true_toMarking_MR) * 6 + 174 - true_toMarking_M * 12) / 2);
            else
                car_pos = int(198 - (true_toMarking_ML + true_toMarking_MR) * 6);

            true_goto_lane = 2;
            if (visualize_angle == 1) {
                pt[0] = cvPoint(p1_x + car_pos, p1_y + 600);
                pt[1] = cvPoint(p2_x + car_pos, p2_y + 600);
                pt[2] = cvPoint(p3_x + car_pos, p3_y + 600);
                pt[3] = cvPoint(p4_x + car_pos, p4_y + 600);
                cvFillConvexPoly(semanticRGB, pt, 4, cvScalar(0, 0, 255));
            } else
                cvRectangle(semanticRGB, cvPoint(car_pos - 14, 600 - 28), cvPoint(car_pos + 14, 600 + 28),
                            cvScalar(0, 0, 255), -1);

            if (true_dist_LL < 60)
                cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - true_dist_LL * 12 - 28),
                            cvPoint(150 + 14, 600 - true_dist_LL * 12 + 28), cvScalar(0, 255, 255), -1);
            if (true_dist_MM < 60)
                cvRectangle(semanticRGB, cvPoint(198 - 14, 600 - true_dist_MM * 12 - 28),
                            cvPoint(198 + 14, 600 - true_dist_MM * 12 + 28), cvScalar(0, 255, 255), -1);
        } else if (true_toMarking_RR < 9 && true_toMarking_LL < -9) {   // left lane

            if (true_toMarking_M < 2 && true_toMarking_L < -6)
                car_pos = int((102 - (true_toMarking_ML + true_toMarking_MR) * 6 + 78 - true_toMarking_M * 12) / 2);
            else if (true_toMarking_M < 2 && true_toMarking_L > -6)
                car_pos = int((102 - (true_toMarking_ML + true_toMarking_MR) * 6 + 126 - true_toMarking_M * 12) / 2);
            else
                car_pos = int(102 - (true_toMarking_ML + true_toMarking_MR) * 6);

            true_goto_lane = 1;
            if (visualize_angle == 1) {
                pt[0] = cvPoint(p1_x + car_pos, p1_y + 600);
                pt[1] = cvPoint(p2_x + car_pos, p2_y + 600);
                pt[2] = cvPoint(p3_x + car_pos, p3_y + 600);
                pt[3] = cvPoint(p4_x + car_pos, p4_y + 600);
                cvFillConvexPoly(semanticRGB, pt, 4, cvScalar(0, 0, 255));
            } else
                cvRectangle(semanticRGB, cvPoint(car_pos - 14, 600 - 28), cvPoint(car_pos + 14, 600 + 28),
                            cvScalar(0, 0, 255), -1);

            if (true_dist_MM < 60)
                cvRectangle(semanticRGB, cvPoint(102 - 14, 600 - true_dist_MM * 12 - 28),
                            cvPoint(102 + 14, 600 - true_dist_MM * 12 + 28), cvScalar(0, 255, 255), -1);
            if (true_dist_RR < 60)
                cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - true_dist_RR * 12 - 28),
                            cvPoint(150 + 14, 600 - true_dist_RR * 12 + 28), cvScalar(0, 255, 255), -1);
        } else if (true_toMarking_RR < 9 && true_toMarking_LL > -9) {   // central lane

            if (true_toMarking_M < 2) {
                if (true_toMarking_ML + true_toMarking_MR > 0)
                    car_pos = int(
                            (150 - (true_toMarking_ML + true_toMarking_MR) * 6 + 126 - true_toMarking_M * 12) / 2);
                else
                    car_pos = int(
                            (150 - (true_toMarking_ML + true_toMarking_MR) * 6 + 174 - true_toMarking_M * 12) / 2);
            } else
                car_pos = int(150 - (true_toMarking_ML + true_toMarking_MR) * 6);

            if (true_toMarking_ML + true_toMarking_MR > 0) true_goto_lane = 1;
            else true_goto_lane = 2;

            if (visualize_angle == 1) {
                pt[0] = cvPoint(p1_x + car_pos, p1_y + 600);
                pt[1] = cvPoint(p2_x + car_pos, p2_y + 600);
                pt[2] = cvPoint(p3_x + car_pos, p3_y + 600);
                pt[3] = cvPoint(p4_x + car_pos, p4_y + 600);
                cvFillConvexPoly(semanticRGB, pt, 4, cvScalar(0, 0, 255));
            } else
                cvRectangle(semanticRGB, cvPoint(car_pos - 14, 600 - 28), cvPoint(car_pos + 14, 600 + 28),
                            cvScalar(0, 0, 255), -1);

            if (true_dist_LL < 60)
                cvRectangle(semanticRGB, cvPoint(102 - 14, 600 - true_dist_LL * 12 - 28),
                            cvPoint(102 + 14, 600 - true_dist_LL * 12 + 28), cvScalar(0, 255, 255), -1);
            if (true_dist_MM < 60)
                cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - true_dist_MM * 12 - 28),
                            cvPoint(150 + 14, 600 - true_dist_MM * 12 + 28), cvScalar(0, 255, 255), -1);
            if (true_dist_RR < 60)
                cvRectangle(semanticRGB, cvPoint(198 - 14, 600 - true_dist_RR * 12 - 28),
                            cvPoint(198 + 14, 600 - true_dist_RR * 12 + 28), cvScalar(0, 255, 255), -1);
        } else if (true_toMarking_M < 3) {  // on lane marking

            if (true_toMarking_L < -6) {   // left
                car_pos = int(78 - true_toMarking_M * 12);
                if (true_dist_R < 60)
                    cvRectangle(semanticRGB, cvPoint(102 - 14, 600 - true_dist_R * 12 - 28),
                                cvPoint(102 + 14, 600 - true_dist_R * 12 + 28), cvScalar(0, 255, 255), -1);
            } else if (true_toMarking_R > 6) {  // right
                car_pos = int(222 - true_toMarking_M * 12);
                if (true_dist_L < 60)
                    cvRectangle(semanticRGB, cvPoint(198 - 14, 600 - true_dist_L * 12 - 28),
                                cvPoint(198 + 14, 600 - true_dist_L * 12 + 28), cvScalar(0, 255, 255), -1);
            } else if (true_goto_lane == 1) {  // central L
                car_pos = int(126 - true_toMarking_M * 12);
                if (true_dist_L < 60)
                    cvRectangle(semanticRGB, cvPoint(102 - 14, 600 - true_dist_L * 12 - 28),
                                cvPoint(102 + 14, 600 - true_dist_L * 12 + 28), cvScalar(0, 255, 255), -1);
                if (true_dist_R < 60)
                    cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - true_dist_R * 12 - 28),
                                cvPoint(150 + 14, 600 - true_dist_R * 12 + 28), cvScalar(0, 255, 255), -1);
            } else if (true_goto_lane == 2) { // central R
                car_pos = int(174 - true_toMarking_M * 12);
                if (true_dist_L < 60)
                    cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - true_dist_L * 12 - 28),
                                cvPoint(150 + 14, 600 - true_dist_L * 12 + 28), cvScalar(0, 255, 255), -1);
                if (true_dist_R < 60)
                    cvRectangle(semanticRGB, cvPoint(198 - 14, 600 - true_dist_R * 12 - 28),
                                cvPoint(198 + 14, 600 - true_dist_R * 12 + 28), cvScalar(0, 255, 255), -1);
            }

            if (visualize_angle == 1) {
                pt[0] = cvPoint(p1_x + car_pos, p1_y + 600);
                pt[1] = cvPoint(p2_x + car_pos, p2_y + 600);
                pt[2] = cvPoint(p3_x + car_pos, p3_y + 600);
                pt[3] = cvPoint(p4_x + car_pos, p4_y + 600);
                cvFillConvexPoly(semanticRGB, pt, 4, cvScalar(0, 0, 255));
            } else
                cvRectangle(semanticRGB, cvPoint(car_pos - 14, 600 - 28), cvPoint(car_pos + 14, 600 + 28),
                            cvScalar(0, 0, 255), -1);
        }
        /////////////////// END draw groundtruth data

        //////////////// visualize angle
        if (visualize_angle == 1) {
            angle = -angle;
            p1_x = -14 * cos(angle) + 28 * sin(angle);
            p1_y = 14 * sin(angle) + 28 * cos(angle);
            p2_x = 14 * cos(angle) + 28 * sin(angle);
            p2_y = -14 * sin(angle) + 28 * cos(angle);
            p3_x = 14 * cos(angle) - 28 * sin(angle);
            p3_y = -14 * sin(angle) - 28 * cos(angle);
            p4_x = -14 * cos(angle) - 28 * sin(angle);
            p4_y = 14 * sin(angle) - 28 * cos(angle);
        }
        //////////////// END visualize angle

        /////////////////// draw sensing data
        if (toMarking_LL > -8 && toMarking_RR > 8 && -toMarking_ML + toMarking_MR < 5.5) {     // right lane

            if (toMarking_M < 1.5 && toMarking_R > 6)
                car_pos = int((198 - (toMarking_ML + toMarking_MR) * 6 + 222 - toMarking_M * 12) / 2);
            else if (toMarking_M < 1.5 && toMarking_R <= 6)
                car_pos = int((198 - (toMarking_ML + toMarking_MR) * 6 + 174 - toMarking_M * 12) / 2);
            else
                car_pos = int(198 - (toMarking_ML + toMarking_MR) * 6);

            goto_lane = 2;
            if (visualize_angle == 1) {
                pt[0] = cvPoint(p1_x + car_pos, p1_y + 600);
                pt[1] = cvPoint(p2_x + car_pos, p2_y + 600);
                pt[2] = cvPoint(p3_x + car_pos, p3_y + 600);
                pt[3] = cvPoint(p4_x + car_pos, p4_y + 600);
                int npts = 4;
                cvPolyLine(semanticRGB, &pt, &npts, 1, 1, cvScalar(0, 255, 0), 2, CV_AA);
            } else
                cvRectangle(semanticRGB, cvPoint(car_pos - 14, 600 - 28), cvPoint(car_pos + 14, 600 + 28),
                            cvScalar(0, 255, 0), 2);

            if (dist_LL < 50)
                cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - dist_LL * 12 - 28),
                            cvPoint(150 + 14, 600 - dist_LL * 12 + 28), cvScalar(237, 99, 157), 2);
            if (dist_MM < 50)
                cvRectangle(semanticRGB, cvPoint(198 - 14, 600 - dist_MM * 12 - 28),
                            cvPoint(198 + 14, 600 - dist_MM * 12 + 28), cvScalar(237, 99, 157), 2);
        } else if (toMarking_RR < 8 && toMarking_LL < -8 && -toMarking_ML + toMarking_MR < 5.5) {   // left lane

            if (toMarking_M < 1.5 && toMarking_L < -6)
                car_pos = int((102 - (toMarking_ML + toMarking_MR) * 6 + 78 - toMarking_M * 12) / 2);
            else if (toMarking_M < 1.5 && toMarking_L >= -6)
                car_pos = int((102 - (toMarking_ML + toMarking_MR) * 6 + 126 - toMarking_M * 12) / 2);
            else
                car_pos = int(102 - (toMarking_ML + toMarking_MR) * 6);

            goto_lane = 1;
            if (visualize_angle == 1) {
                pt[0] = cvPoint(p1_x + car_pos, p1_y + 600);
                pt[1] = cvPoint(p2_x + car_pos, p2_y + 600);
                pt[2] = cvPoint(p3_x + car_pos, p3_y + 600);
                pt[3] = cvPoint(p4_x + car_pos, p4_y + 600);
                int npts = 4;
                cvPolyLine(semanticRGB, &pt, &npts, 1, 1, cvScalar(0, 255, 0), 2, CV_AA);
            } else
                cvRectangle(semanticRGB, cvPoint(car_pos - 14, 600 - 28), cvPoint(car_pos + 14, 600 + 28),
                            cvScalar(0, 255, 0), 2);

            if (dist_MM < 50)
                cvRectangle(semanticRGB, cvPoint(102 - 14, 600 - dist_MM * 12 - 28),
                            cvPoint(102 + 14, 600 - dist_MM * 12 + 28), cvScalar(237, 99, 157), 2);
            if (dist_RR < 50)
                cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - dist_RR * 12 - 28),
                            cvPoint(150 + 14, 600 - dist_RR * 12 + 28), cvScalar(237, 99, 157), 2);
        } else if (toMarking_RR < 8 && toMarking_LL > -8 && -toMarking_ML + toMarking_MR < 5.5) {   // central lane

            if (toMarking_M < 1.5) {
                if (toMarking_ML + toMarking_MR > 0)
                    car_pos = int((150 - (toMarking_ML + toMarking_MR) * 6 + 126 - toMarking_M * 12) / 2);
                else
                    car_pos = int((150 - (toMarking_ML + toMarking_MR) * 6 + 174 - toMarking_M * 12) / 2);
            } else
                car_pos = int(150 - (toMarking_ML + toMarking_MR) * 6);

            if (toMarking_ML + toMarking_MR > 0) goto_lane = 1;
            else goto_lane = 2;

            if (visualize_angle == 1) {
                pt[0] = cvPoint(p1_x + car_pos, p1_y + 600);
                pt[1] = cvPoint(p2_x + car_pos, p2_y + 600);
                pt[2] = cvPoint(p3_x + car_pos, p3_y + 600);
                pt[3] = cvPoint(p4_x + car_pos, p4_y + 600);
                int npts = 4;
                cvPolyLine(semanticRGB, &pt, &npts, 1, 1, cvScalar(0, 255, 0), 2, CV_AA);
            } else
                cvRectangle(semanticRGB, cvPoint(car_pos - 14, 600 - 28), cvPoint(car_pos + 14, 600 + 28),
                            cvScalar(0, 255, 0), 2);

            if (dist_LL < 50)
                cvRectangle(semanticRGB, cvPoint(102 - 14, 600 - dist_LL * 12 - 28),
                            cvPoint(102 + 14, 600 - dist_LL * 12 + 28), cvScalar(237, 99, 157), 2);
            if (dist_MM < 50)
                cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - dist_MM * 12 - 28),
                            cvPoint(150 + 14, 600 - dist_MM * 12 + 28), cvScalar(237, 99, 157), 2);
            if (dist_RR < 50)
                cvRectangle(semanticRGB, cvPoint(198 - 14, 600 - dist_RR * 12 - 28),
                            cvPoint(198 + 14, 600 - dist_RR * 12 + 28), cvScalar(237, 99, 157), 2);
        } else if (toMarking_M < 2.5) {  // on lane marking

            if (toMarking_L < -6) {   // left
                car_pos = int(78 - toMarking_M * 12);
                if (dist_R < 50)
                    cvRectangle(semanticRGB, cvPoint(102 - 14, 600 - dist_R * 12 - 28),
                                cvPoint(102 + 14, 600 - dist_R * 12 + 28), cvScalar(237, 99, 157), 2);
            } else if (toMarking_R > 6) {  // right
                car_pos = int(222 - toMarking_M * 12);
                if (dist_L < 50)
                    cvRectangle(semanticRGB, cvPoint(198 - 14, 600 - dist_L * 12 - 28),
                                cvPoint(198 + 14, 600 - dist_L * 12 + 28), cvScalar(237, 99, 157), 2);
            } else if (goto_lane == 1) {  // central L
                car_pos = int(126 - toMarking_M * 12);
                if (dist_L < 50)
                    cvRectangle(semanticRGB, cvPoint(102 - 14, 600 - dist_L * 12 - 28),
                                cvPoint(102 + 14, 600 - dist_L * 12 + 28), cvScalar(237, 99, 157), 2);
                if (dist_R < 50)
                    cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - dist_R * 12 - 28),
                                cvPoint(150 + 14, 600 - dist_R * 12 + 28), cvScalar(237, 99, 157), 2);
            } else if (goto_lane == 2) { // central R
                car_pos = int(174 - toMarking_M * 12);
                if (dist_L < 50)
                    cvRectangle(semanticRGB, cvPoint(150 - 14, 600 - dist_L * 12 - 28),
                                cvPoint(150 + 14, 600 - dist_L * 12 + 28), cvScalar(237, 99, 157), 2);
                if (dist_R < 50)
                    cvRectangle(semanticRGB, cvPoint(198 - 14, 600 - dist_R * 12 - 28),
                                cvPoint(198 + 14, 600 - dist_R * 12 + 28), cvScalar(237, 99, 157), 2);
            }

            if (visualize_angle == 1) {
                pt[0] = cvPoint(p1_x + car_pos, p1_y + 600);
                pt[1] = cvPoint(p2_x + car_pos, p2_y + 600);
                pt[2] = cvPoint(p3_x + car_pos, p3_y + 600);
                pt[3] = cvPoint(p4_x + car_pos, p4_y + 600);
                int npts = 4;
                cvPolyLine(semanticRGB, &pt, &npts, 1, 1, cvScalar(0, 255, 0), 2, CV_AA);
            } else
                cvRectangle(semanticRGB, cvPoint(car_pos - 14, 600 - 28), cvPoint(car_pos + 14, 600 + 28),
                            cvScalar(0, 255, 0), 2);
        }
        /////////////////// END draw sensing data

        if ((shared->control == 0) || (manual == 1)) {
            for (int h = 0; h < SEMANTIC_HEIGHT; h++) {
                for (int w = 0; w < SEMANTIC_WIDTH; w++) {
                    semanticRGB->imageData[(h * SEMANTIC_WIDTH + w) * 3 + 1] = 0;
                    semanticRGB->imageData[(h * SEMANTIC_WIDTH + w) * 3 + 0] = 0;
                }
            }
        }
    }

    cvShowImage("Semantic Visualization",semanticRGB);
    cvWaitKey(1);
    ///////////////////////////// END semantic visualization
}
