#!/bin/bash

echo "Cleaning target directory..."
rm -rf target/*

echo "Generating code..."
java -jar embedded-montiarc-math-middleware-generator-0.0.18-SNAPSHOT-jar-with-dependencies.jar project_dpnet.json

echo "Copying build and run scripts..."
cp -rf resources/scripts/* target/
chmod u+x target/*.sh

echo "Copying CNN weights..."
cp -rf resources/TORCSComponent target/TORCSComponent
chmod u+x resources/TORCSComponent/*.sh
cp -rf resources/dpnet target/model

echo "Building generated code..."
cd target
./build_all.sh

echo "Please start TORCS, configure race and run ./run_all.sh"
