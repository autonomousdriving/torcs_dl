#!/usr/bin/env zsh

### Job name
#BSUB -J TrainDPNET

### File / path where STDOUT & STDERR will be written
###    %J is the job ID, %I is the array ID
#BSUB -o train_dpnet.%J.%I

#BSUB -B
#BSUB -N
#BSUB -u svetlana.pavlitskaya@rwth-aachen.de

### Request the time you need for execution in minutes
### Format [hour:]minute,
### that means for 80 minutes you could also use this: 1:20
#BSUB -W 10:00

### Request vitual memory (in MB)
#BSUB -M 4096

### Request GPU Queue
#BSUB -gpu -
#BSUB -R gpu


module load cuda

hostname
nvcc --version

export MXNET_CPU_WORKER_NTHREADS=32
export MXNET_ENGINE_TYPE=ThreadedEnginePerDevice

cd /home/sp705423/GeneratedDpnetTrainingCode
pip install --user -r requirements.txt
python CNNTrainer_Dpnet.py
