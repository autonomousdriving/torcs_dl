import mxnet as mx
import numpy as np

DATA_NAME = 'data'
PREDICTIONS_LABEL = 'predictions_label'

TRAIN_MEAN = [99.39394537, 110.60877108, 117.86127587]
TRAIN_STD = [42.04910545, 49.47874084, 62.61726178]

def load_data_rec(data_dir, batch_size):
    width = 280
    height = 210

    data_mean = np.load("./mean_image.npy")
    train_iter = mx.image.ImageIter(
        path_imgrec=data_dir + "torcs_train.rec",
        data_shape=(3, height, width),  # (channels, height, width)
        batch_size=batch_size,
        label_width=14,
        data_name=DATA_NAME,
        label_name=PREDICTIONS_LABEL
    )
    test_iter = mx.image.ImageIter(
        path_imgrec=data_dir + "torcs_test.rec",
        data_shape=(3, height, width),  # (channels, height, width)
        batch_size=batch_size,
        label_width=14,
        data_name=DATA_NAME,
        label_name=PREDICTIONS_LABEL
    )

    data_std = None
    # data_mean = np.asarray([[[a] * width] * height for a in TRAIN_MEAN])
    # data_std = np.asarray([[[a] * width] * height for a in TRAIN_STD])

    return train_iter, test_iter, data_mean, data_std
